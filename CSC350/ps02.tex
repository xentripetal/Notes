\documentclass[11pt]{article} 

% ------- List of packages to import ------------
\usepackage{fullpage}
\usepackage{soul}
\usepackage{url}
\usepackage{pgf}
\usepackage{tikz}
\usetikzlibrary{arrows,automata}
\usepackage[latin1]{inputenc}
\usepackage{amssymb}
\usepackage{mathtools}          %loads amsmath as well
\RequirePackage[amsmath,hyperref,thmmarks]{ntheorem}
\usepackage{hyperref} % This always has to be the LAST package.  

\theoremseparator{.}

% ------- Theorem and related environments --------
\newtheorem{theorem}{Theorem}
\newtheorem{conjecture}[theorem]{Conjecture}
\newtheorem{proposition}[theorem]{Proposition}
\newtheorem{claim}[theorem]{Claim}
\newtheorem{lemma}[theorem]{Lemma}
\newtheorem{corollary}[theorem]{Corollary}
\newtheorem{definition}[theorem]{Definition} 
\newtheorem{problem}[theorem]{Problem}
\newtheorem{observation}[theorem]{Observation}
\newtheorem{fact}[theorem]{Fact}

% ------- Commands specifying theorem style -------

\DeclarePairedDelimiter\Floor\lfloor\rfloor
\DeclarePairedDelimiter\Ceil\lceil\rceil

\theoremstyle{nonumberplain}
\theoremheaderfont{\normalfont\itshape}
\theorembodyfont{\normalfont}
\theoremsymbol{\ensuremath{_\blacksquare}}
\newtheorem{proof}{Proof}

% ------- Useful math macros -----------
\newcommand{\NN}{\mathcal{N}} % Natural Numbers
\newcommand{\RR}{\mathcal{R}} % Real Numbers
\newcommand{\ZZ}{\mathcal{Z}} % Integers
\newcommand{\QQ}{\mathcal{Q}} % Rational Numbers

\newcommand{\set}[1]{\ensuremath{\{{#1}\}}} % Set
\newcommand{\bigset}[1]{\ensuremath{\left\{{#1}\right\}}}
\newcommand{\condset}[2]{\ensuremath{\set{{#1}\;|\;{#2}}}} % Conditional set
\newcommand{\nin}{\not\in}
\newcommand{\cross}{\times} % Cartesian product
\newcommand{\ssn}{\subsetneq} % Proper subset
\newcommand{\sse}{\subseteq} % Subset




\begin{document}

%% ^^^^^^^^^ Don't change anything above this line ^^^^^^^^^^^^^^^^



\begin{center}

{\LARGE
\textsc{CSC 350 -- Theory of Computing -- Problem Set 2}
\bigskip}

Due Wednesday, April 12$^{th}$ before class

\bigskip
{\Large
Corbin Martin
}


\end{center}

\noindent Solve the following problems and typeset your solutions
within this document.  To hand in your solution: (i) electronically
submit \texttt{ps02.pdf} and \texttt{ps02.tex} to Nexus, and (ii) turn
in a print out of \texttt{ps02.pdf} at the beginning of class.


\section*{Proving regularity}

\subsection*{Problem 1}

Let $\Sigma = \{0,1\}$ and let

$$
D = \{ w | w\textnormal{ contains an equal number of occurrences of
  the substrings }01\textnormal{ and }10\}
$$ 

That means, $101 \in D$ because $101$ contains a single $01$ and a
single $10$, but $1010 \not\in D$ because $1010$ contains two $10$'s
and only one $01$. Show that $D$ is a regular language.

\begin{center}
\begin{tikzpicture}[->,>=stealth',shorten >=1pt,auto,node distance=3cm,
                    thick]
  \tikzstyle{every state}=[fill=white,draw=black,text=black]
  \node[initial,state] (q1)               {$q_1$};
  \node[state,accepting]         (q2) [above right of=q1] {$q_2$};
  \node[state,accepting]         (q4) [below right of=q1] {$q_4$};
  \node[state]         (q3) [right of=q2] {$q_3$};
  \node[state]         (q5) [right of=q4] {$q_5$};
  \path 
        (q1) edge [bend left]  node {0} (q2)
        (q1) edge [bend right]  node {1} (q4)
		(q2) edge [loop above] node {0} (q2)
		(q2) edge [bend left] node {1} (q3)
		(q3) edge [bend left] node {0} (q2)
		(q3) edge [loop above] node {1} (q3)
		(q4) edge [loop above] node {1} (q4)
		(q4) edge [bend left] node {0} (q5)
		(q5) edge [bend left] node {1} (q4)
		(q5) edge [loop above] node {0} (q5);
		
\end{tikzpicture}\\
\end{center}

\subsection*{Problem 2}

Let $B_n = \{ a^k | k\textnormal{ is a multiple of n}\}$. Show that
for each $n \geq 1$, the language $B_n$ is regular. That means, you
have to prove that the languages $B_1, B_2, B_3$, etc. are all
regular. \emph{Hint:} Use induction over $n$.

\paragraph{Basis: Let $n=1$, thus $B_1 = \{a^k | k$ is a mutiple of $1\}$. As every whole number is a mutiple of 1,  this means $B_1 = \{a^k | k \in \mathcal{N}\}$ and because it is impossible to have k be a negative number, this is the same as $B_1 = \{a^k | k \geq 0\}$. This is equivalent to $B^*$ which is a regular operation. Thus $B_1$ is regular.}

\paragraph{Induction Hypothesis: We will assume $B_n$ is a regular operation}

\paragraph{Induction Step: We must show that $B_{n+1}$ is a regular operation. Thus, $B_{n+1} = \{a^k | k$ is a multiple of $n+1\}$. As $B_n$ is regular, there exists a FSA $M_n = (\Sigma_n, Q_n, q_s_n, F_n, \delta_n)$ that recognizes it. Thus, we can construct a new FSA $M$ that copies $M_n$ but has a $\varepsilon$ transition from all accepting states of $M_N$ to a new state that will have a transition to another new state that will be the only accepting state of $M$, which will return to the starting state on any input. Thus, if something was a multiple of $n$, it must now be a multiple of $n+1$. This can be formally defined as}

\begin{enumerate}
	\item $\Sigma = \Sigma_n$
	\item $Q = Q_n \cup {q_m, q_a}$
	\item $q_s = q_s_n$
	\item $F = \{q_a\}$
	\item $\delta = \delta_n \cup \{(q_m, \Sigma) \rightarrow q_a, (q_a, \Sigma) \rightarrow q_s\} \cup \{(q_f, \varepsilon) \rightarrow q_m | q_f \in F_n\}$
\end{enumerate}

\paragraph{Thus there exists a FSA for $B_{n+1}$, proving that $B_{n+1}$ is regular. This proves $B_n$ is regular for all $n \geq 1$.}


\section*{Finite state transducers}

\subsection*{Problem 3}

A \emph{finite state transducer} (FST) is a type of deterministic
finite automaton whose output is a string and not just \textit{accept}
or \textit{reject}. For example, the following are state diagrams of
FST's $T_1$ and $T_2$.\\

\begin{minipage}{0.4\textwidth}
\begin{center}
\begin{tikzpicture}[->,>=stealth',shorten >=1pt,auto,node distance=2.5cm,
                    thick]
  \tikzstyle{every state}=[fill=white,draw=black,text=black]
  \node[initial,state] (q1)               {$q_1$};
  \node[state]         (q2) [right of=q1] {$q_2$};
  \path 
        (q1) edge [loop above] node {\parbox{0.5cm}{0/0\\1/0}} (q1)
             edge [bend left]  node {2/1}     (q2)
        (q2) edge [loop above] node {\parbox{0.5cm}{1/1\\2/1}} (q2)
             edge [bend left]  node {0/0} (q1);
\end{tikzpicture}\\
$T_1$
\end{center}
\end{minipage}
\begin{minipage}{0.5\textwidth}
\begin{center}
\begin{tikzpicture}[->,>=stealth',shorten >=1pt,auto,node distance=3.5cm,
                    thick]
  \tikzstyle{every state}=[fill=white,draw=black,text=black]
  \node[initial,state] (q1)               {$q_1$};
  \node[state]         (q3) [below left of=q1] {$q_3$};
  \node[state]         (q2) [below right of=q1] {$q_2$};
  \path 
        (q1) edge [bend left] node {a/1} (q2)
             edge             node {b/1} (q3)
        (q2) edge [bend left] node {a/1} (q3)
             edge             node {b/0} (q1)
        (q3) edge [bend left] node {a/0} (q1)
             edge             node {b/1} (q2);
\end{tikzpicture}\\
$T_2$
\end{center}
\end{minipage}
\vspace{2ex}

Each transition of an FST is labeled with two symbols, one designating
the input symbol and the other designating the output symbol. For
example, in $T_1$, the transition from $q_1$ to $q_2$ has input symbol
$2$ and output symbol $1$. 

When an FST computes on an input string $w$, it takes the input
symbols $w_1...w_n$ one by one and, starting at the start state,
follows the transitions by matching the input symbol with the symbols
from $w$. Each time it makes a transition and consumes an input symbol
from $w$, it outputs the corresponding output symbol. For example, on
input $2212011$, machine $T_1$ enters the sequence of states
$q_1,q_2,q_2,q_2,q_2,q_1,q_1,q_1$ and produces output $1111000$. On
input $abbb$, $T_2$ outputs 1011.

Give the sequence of states entered and the output produced when
running the specified FST on the following inputs:

  %% Add your answers the enumerate environment.

\begin{enumerate}
\item $T_1$ on input $011 = 000$
\item $T_1$ on input $211 = 111$
\item $T_1$ on input $121 = 011$
\item $T_1$ on input $0202 = 0101$
\item $T_2$ on input $b = 1$
\item $T_2$ on input $bbab = 1111$
\item $T_2$ on input $bbbbbb = 110110$
\item $T_2$ on input $\varepsilon = \varepsilon$
\end{enumerate}


\subsection*{Problem 4}

Now give a formal definition of finite state transducers, following
the pattern that we have used in class to define deterministic and
non-deterministic FSA. Assume that an FST has an input alphabet
$\Sigma$ and an output alphabet $\Gamma$ but not a set of accept
states.

A finite state transducer is a 5 tuple automata such that 
\begin{enumerate}
	\item $\Sigma$ is a set of input alphabet
	\item $\Gamma$ is a set of output alphabet
	\item $Q$ is a set of all states
	\item $q_s$ is the initial starting state
	\item $\delta = Q\cross \Sigma \rightarrow Q \cross \{\Gamma \cup \varepsilon\}$
\end{enumerate}

\subsection*{Problem 5}

Using the solution you gave to Problem 4, give a formal description of
the machines $T_1$ and $T_2$ given above.

\begin{enumerate}
	\item $T_1$
		\subitem $\Sigma = \{0, 1, 2\}$
		\subitem $\Gamma = {0, 1}$
		\subitem $Q = {q_1, q_2}$
		\subitem $q_s = q_1$
		\subitem $\delta = \{(q_1, 0)\Rightarrow (q_1, 0),
							(q_1, 1)\Rightarrow (q_1, 0),
							(q_1, 2)\Rightarrow (q_2, 1),
							(q_2, 1)\Rightarrow (q_2, 1),
							(q_2, 2)\Rightarrow (q_2, 1),
							(q_2, 0)\Rightarrow (q_1, 0)\}$
	\item $T_2$
		\subitem $\Sigma = \{a, b\}$
		\subitem $\Gamma = {0, 1}$
		\subitem $Q = {q_1, q_2, q_3}$
		\subitem $q_s = q_1$
		\subitem $\delta = \{(q_1, b)\Rightarrow (q_3, 1), 
							(q_1, a)\Rightarrow (q_2, 1),
							(q_2, a)\Rightarrow (q_3, 1),
							(q_2, b)\Rightarrow (q_1, 0),
							(q_3, a)\Rightarrow (q_1, 0),
							(q_3, b)\Rightarrow (q_2, 1)\}$
\end{enumerate}

\subsection*{Problem 6}

Give the state diagram of an FST with the following behavior: Its
input and output alphabet are $\{0,1\}$. Its output string is
identical to the input string on the even positions but inverted at
the odd positions. For example, on input $0000111$ it should output
$1010010$.

\begin{center}
\begin{tikzpicture}[->,>=stealth',shorten >=1pt,auto,node distance=4cm,
                    thick]
  \tikzstyle{every state}=[fill=white,draw=black,text=black]
  \node[initial,state] (q1)               {$q_1$};
  \node[state]         (q2) [right of=q1] {$q_2$};
  \path 
		(q1) edge [bend left=20] node [above] {0/1} (q2)
		(q1) edge [bend left=50] node [above]{1/0} (q2)
		(q2) edge [bend left=20] node [above]{1/1} (q1)
		(q2) edge [bend left=50] node [above]{0/0} (q1);
\end{tikzpicture}\\
\end{center}


\section*{Closure properties}

\subsection*{Problem 7}

For languages $A$ and $B$, let the \emph{perfect shuffle} of the $A$
and $B$ be the language

$$
\{ w | w = a_1b_1\ldots a_kb_k \textnormal{ where each } a_i, b_i \in
\Sigma \textnormal{ and } a_1\ldots a_k\in A \textnormal{ and }
b_1\ldots b_k\in B\}
$$

For example, if the string $01011 \in A$ and $22233 \in B$, then
$0212021313$ is in the perfect shuffle of $A$ and $B$. Show that the
class of regular languages is closed under perfect shuffle.
\\

Let $M_A$ and $M_B$ be the FSA that recognize $A$ and $B$ respectively. They also each contain the 5 tuple properties by the same denotion, eg. $M_A$ contains $\delta_A$.
We must construct a NFA $M$ that can keep can run both $M_A$ and $M_B$ with alternating steps. So, the states in the machine will be 3 tuple, containing its state in $M_A$, its state in $M_B$, and the next state to run in $\{A, B\}$. The accepting states have it such that $M_A$ and $M_B$ are in accepting states and the next state to run in would be $A$ as $A$ is read from first. Thus, we can define $M$ as such:

\begin{enumerate}
	\item $\Sigma = \Sigma_A \cup \Sigma_B$
	\item $Q = Q_A \cross Q_B \cross \{A, B\}$
	\item $q_s = (q_A_s , q_B_s , A)$ such that we are at starting of $M_A$ and $M_B$ with $A$ to be read
	\item $F = F_A \cross F_B \cross {A}$
	\item $\delta: Q \cross Sigma \rightarrow Q$ where 

\[
  \delta((q_a, q_b, S), i) =
  \begin{cases}
	(\delta_A(q_a, i), q_b, B) & \text{if $S=A$} \\
	(q_a, \delta_B(q_b, i), A)& \text{if $S=B$}
  \end{cases}
\]
\end{enumerate}

Thus, we have constructed a FSA for perfect shuffle, showing that the class of regular languages are closed under pefect shuffle.:wq


%% VVVVVVVVVVV Don't change anything below this line VVVVVVVVVVVVVVV

\end{document}
