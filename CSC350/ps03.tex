\documentclass[11pt]{article} 

% ------- List of packages to import ------------
\usepackage{fullpage}
\usepackage{soul}
\usepackage{url}
\usepackage{pgf}
\usepackage{tikz}
\usetikzlibrary{arrows,automata}
\usepackage[latin1]{inputenc}
\usepackage{amssymb}
\usepackage{amsmath}
\RequirePackage[amsmath,hyperref,thmmarks]{ntheorem}
\usepackage{hyperref} % This always has to be the LAST package.  

\theoremseparator{.}

% ------- Theorem and related environments --------
\newtheorem{theorem}{Theorem}
\newtheorem{conjecture}[theorem]{Conjecture}
\newtheorem{proposition}[theorem]{Proposition}
\newtheorem{claim}[theorem]{Claim}
\newtheorem{lemma}[theorem]{Lemma}
\newtheorem{corollary}[theorem]{Corollary}
\newtheorem{definition}[theorem]{Definition} 
\newtheorem{problem}[theorem]{Problem}
\newtheorem{observation}[theorem]{Observation}
\newtheorem{fact}[theorem]{Fact}

% ------- Commands specifying theorem style -------

\theoremstyle{nonumberplain}
\theoremheaderfont{\normalfont\itshape}
\theorembodyfont{\normalfont}
\theoremsymbol{\ensuremath{_\blacksquare}}
\newtheorem{proof}{Proof}

\newcounter{casenum}
\newenvironment{caseof}{\setcounter{casenum}{1}}{\vskip.5\baselineskip}
\newcommand{\case}[2]{\vskip.5\baselineskip\par\noindent {\bfseries Case \arabic{casenum}:} #1\\#2\addtocounter{casenum}{1}}

% ------- Useful math macros -----------
\newcommand{\NN}{\mathcal{N}} % Natural Numbers
\newcommand{\RR}{\mathcal{R}} % Real Numbers
\newcommand{\ZZ}{\mathcal{Z}} % Integers
\newcommand{\QQ}{\mathcal{Q}} % Rational Numbers

\newcommand{\set}[1]{\ensuremath{\{{#1}\}}} % Set
\newcommand{\bigset}[1]{\ensuremath{\left\{{#1}\right\}}}
\newcommand{\condset}[2]{\ensuremath{\set{{#1}\;|\;{#2}}}} % Conditional set
\newcommand{\nin}{\not\in}
\newcommand{\cross}{\times} % Cartesian product
\newcommand{\ssn}{\subsetneq} % Proper subset
\newcommand{\sse}{\subseteq} % Subset




\begin{document}

%% ^^^^^^^^^ Don't change anything above this line ^^^^^^^^^^^^^^^^



\begin{center}

{\LARGE
\textsc{CSC 350 -- Theory of Computing -- Problem Set 3}
\bigskip}

Due Wednesday, April 19$^{th}$ before class

\bigskip
{\Large
Corbin Martin
}


\end{center}

\noindent Solve the following problems and typeset your solutions
within this document.  To hand in your solution: (i) electronically
submit \texttt{ps03.pdf} and \texttt{ps03.tex} to Nexus, and (ii) turn
in a print out of \texttt{ps03.pdf} at the beginning of class.


\subsection*{Problem 1}

\begin{itemize}
\item Give a regular expression that describes the following language over
  the alphabet $\{0,1\}$:
  $$
  \{ w | w \text{ every odd position of } w \text{ is a } 1\}
  $$

\fbox{
	$$(1 (1\|0))^*(1|\varepsilon) $$
}

\item Give a regular expression that describes the following language over
  the alphabet $\{0,1\}$:
  $$
  \{ w | w \text{ starts with } 0 \text{ and has odd length or } w
  \text{ starts with } 1 \text{ and has even length}\}
  $$

\fbox{
	$$\left( 0 \left( \varepsilon | \left(\Sigma \Sigma \right)^* \right)\right) |  
	   \left( 1 \Sigma \left(\Sigma \Sigma \right)^*\right)	
	$$
}


\end{itemize}

\subsection*{Problem 2}

Let the \textbf{\textit{avoids}} operation for two languages $A$ and
$B$ be defined as follows:

$$
A\ avoids\ B = \{ w | w \in A \text{ and } w \text{ doesn't contain any
  string in } B \text{ as a substring}\}
$$

Prove that the class of regular languages is closed under the $avoids$
operation. (\textit{Hint:} To prove this, you can either use an
approach similar to the closure proofs that we have done in class, or
you can make an argument based on the closure properties that we have
already proved. If you want to take the first approach, review the
construction we used to prove that any NFSA is equivalent to a DFSA.)

\fbox{\begin{minipage}{40em}

We know that all strings containing any string in B as a substring can
be defined as $(\Sigma ^* B \Sigma ^*)$, which is a regular expression,
therefore any closed operations with it are closed. So, the set of all
strings in A that contain a substring of any string in B can be written
as $A \cap (\Sigma ^* B \Sigma ^*)$. This is also closed as intersection
is a closed operation. So, all we have to do is take the colliding strings
and remove them from $A$ to get a $A\ avoids\ B$. Thus,

$$A\ avoids\ B = A - A \cap (\Sigma ^* B \Sigma ^*)$$

and subtraction is also closed. Therefore, $avoids$ is closed.

\end{minipage}}

\subsection*{Problem 3}

Use the pumping lemma to prove that the following language is not
regular.
$$
\{ uu^{\mathcal{R}} | u \in \{0,1\}^*\}
$$


\fbox{\begin{minipage}{40em}
	Let $B = \{ uu^{\mathcal{R}} |\ u \in \{0,1\}^*\}$. We will assume $B$
	is regular, therefore the pumping lemma will hold. So, let $P$ be the
	pumping length. We know that the pumping lemma applies to any string
	$s \in B$ where $\left| s \right| \geq P$. So, we will choose $s$ to be
	$s = 0^P110^P$, note that $u = 0^P1$. According to the pumping lemma, 
	there is a way of splitting S into 3 pieces $s=xyz$ such that 
	$xy^iz \in B, i \geq 0$. Note that the pumping lemma states 
	$\left| xy \right| \leq P$. So, y must be in the first subsequence of 0's.
	However, $xy^2z \not \in B$ as the first half would not be equal to the 
	reverse of the second half due to an inequal number of 0's. Contradiction. So the pumping lemma does not
	hold for $B$. Therefore, $B$ is not regular.
\end{minipage}}

\subsection*{Problem 4}

Let $M_1$ and $M_2$ be deterministic FSAs that have $k_1$ and $k_2$
states, respectively, and then let $U = L(M_1) \cup L(M_2)$.

\begin{enumerate}
\item Show that if $U \neq \emptyset$, the $U$ contains some string
  $s$, where $|s| < max(k_1, k_2)$.

		\textbf{Note that we are using path and sequence interchangably, and to
		map a path/sequence to a string is to turn the sequence's required input
		into a string.}

\fbox{\begin{minipage}{40em}
	We know that $U \neq \emptyset$ and $U = L(M_1) \cup L(M_2)$. So 
	$L(M_1) \neq \emptyset$ or $L(M_2) \neq \emptyset$
	\begin{caseof}
		\case{$L(M_1) \neq \emptyset, L(M_2) = \emptyset$}{
			So, $M_1$ has some accepting states and $M_2$ has none. 
			Thus, we can disregard $M_2$. We know that there exists
			some path in $M_1$ from the starting state to atleast 1 accepting
			state as $L(M_1) \neq \emptyset$. This path can be taken in
			the most direct route, which would not return to any states
			already taken. This path would be atmost $k_1-1$ transitions
			as the starting state is not considered. Therefore, we can
			consider $s \in U$ where $s$ maps to this path. Thus,
			$\left| s \right| < k_1 = max(k_1, k_2)$.
			}
		\case{$L(M_1) \eq \emptyset, L(M_2) \neq \emptyset$}{
			The same as case 1, but swapping $L(M_1)$ with $L(M_2)$ and $k_1$ with $k_2$. 
			}
		\case{$L(M_1) \neq \emptyset, L(M_2) \neq \emptyset$}{
			We know there exists some path in $M_1$ and $M_2$ to atleast
			1 accepting state. So we will repeat our steps in case 1, choosing
			to disregard $M_2$. Thus if the resulting length, $\left| s \right|$,
			is less than $k_1$. So, if $max(k_1, k_2) = k_1$ then 
			$\left| S \right| < max(k_1, k_2)$. Alternatively, if $max(k_1, k_2) = k_2$
			then $\left| s \right| < k_1 < k_2 = max(k_1, k_2)$. 
			}
	\end{caseof}
	So in each case, there exists a $s$ such that $\left| s \right| < max(k_1, k_2)$.
\end{minipage}}

\pagebreak
\item Show that if $U \neq \Sigma^*$, then $U$ excludes some string
  $s$, where $|s| < k_1k_2$.

\fbox{\begin{minipage}{40em}
	Let $M$ be the dFSA for $U$ using the construction of $M_1 \cross M_2$.
	as $M$ is a dFSA's, every single state has transitions for every possible input.
	Therefore, if every state was an accepting state, every possible input would be
	accepted, resulting in the language being $\Sigma^*$. However, we know that
	$U \neq \Sigma^*$. Therefore, there is atleast 1 non accepting state in $U$ and
	there exists a sequence to this state. We also know that the most direct path to
	this state is atmost the number of states in the FSA, excluding the start state.
	By our construction, there are $k_1 \times k_2$ states. So, if we consider $s$
	where $s$ maps to this path, $\left| s \right| < k_1k_2$ and $s \not \in U$.
	Therefore, $U$ excludes $s$. 

\end{minipage}}

\end{enumerate}


%% VVVVVVVVVVV Don't change anything below this line VVVVVVVVVVVVVVV

\end{document}
