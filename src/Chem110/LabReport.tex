\documentclass{article}
\usepackage[margin=1in]{geometry}
\usepackage[version=3]{mhchem} % Package for chemical equation typesetting
\usepackage{siunitx} % Provides the \SI{}{} and \si{} command for typesetting SI units
\usepackage{amsmath} % Required for some math elements 
\usepackage[english]{babel}
\usepackage{float}
\errorcontextlines 10000

\restylefloat{table}
%\setlength\parindent{5pt} % Removes all indentation from paragraphs

\renewcommand{\labelenumi}{\alph{enumi}} % Make numbering in the enumerate environment by letter rather than number (e.g. section 6)

%\usepackage{times} % Uncomment to use the Times New Roman font

%----------------------------------------------------------------------------------------
%	DOCUMENT INFORMATION
%----------------------------------------------------------------------------------------

\title{Hot Packs, Cold Packs, and Thermochemistry \\ Lab Report \\ CHEM 110H} % Title

\author{Corbin \textsc{Martin}} % Author name


\date{\today} % Date for the report

\begin{document}

\maketitle % Insert the title, author and date

\begin{center}
\begin{tabular}{l r}
	Dates Performed: & September 8, 2016 \\ 
	& September 12, 2016 \\ % Date the experiment was performed
	Partner: & Anna Mahony \\ % Partner names
	Instructor: & Professor Anderson % Instructor/supervisor
\end{tabular}
\end{center}


{\emph I affirm that I have carried out my academic endeavors with full academic honesty.}\\

{\centering
Name \underline{\hspace{5cm}}
\hspace{2cm}
Signature \underline{\hspace{5cm}}
}


%----------------------------------------------------------------------------------------
%	SECTION 1
%----------------------------------------------------------------------------------------

\section{Introduction}

This experiment was split into two parts where one supplied required data for the next. 
The first part is based on the fact that a calorimeter absorbs a certain amount of heat in an reaction which can cause errors in energy and heat calculations for experiments.
So by determining the heat calorimeter constant ($C_{cal}$) it can be accounted for in calculations. 
So the first experiment attempts to determine the $C_{cal}$ by running two tests of adding hot water to cold water and using the change in temperature for collecting an average $C_{cal}$.
This is done through the equation of $0 = q_{rxn}+q_{solution}+q_{calorimeter}$, which can be extended as $0 = (\frac{mass_{rxn}}{g/mol_{rxn}} \times C_{p,rxn} \times \Delta T_{rxn}) + (\frac{mass_{solution}}{g/mol_{solution}} \times C_{p,solution} \times \Delta T_{solution} ) + (C_{cal} \times \Delta T_{cal})$. 
Therefore, by finding the proper values and solving for $C_{cal}$ you can determine the heat absorption constant caused by a unique calorimeter.\\

The second part uses the previously determined $C_{cal}$ to calculate the change in enthalpy of absorbing $CaCl_2$ into water. It can be calculated theoretically using the predetermined $\Delta H_f^\circ$ values of aqueous calcium, aqueous chlorine, and solid calcium chloride and plugging them into the Hess's Law equation of $\Delta H = \sum \Delta H_{f, products}^\circ - \sum \Delta H_{f, reactions}^\circ$. However, the goal is to compare this theoretical value to a real world value using our $C_{cal}$ and determine a percent error. To do this, the equation of $\Delta H = \Delta E + \Delta (PV)$ is used, however it can be substituted to $\Delta H = q_{rxn}+q_{solution}+q_{calorimeter}$ given that one can assume constant pressure, leaving just the $\Delta E$.


 
%----------------------------------------------------------------------------------------
%	SECTION 2
%----------------------------------------------------------------------------------------

\section{Experimental}
For the first set of experiments around half a cup of room temperature water was filled into two combined previously weighed styrofoam cups, acting as a calorimeter. The water's temperature was measured to verify it was at room temperature. The mass of the water and cups was then weighed to calculate the mass of the water. Next around a fourth of a cup of water was heated to above 60$^\circ$C and temperature was recorded. The heated water was added to the room temperature water and the combined temperature was recorded after being stirred to reach equilibrium. Finally the combined waters were weighed to determine the mass of the hot water.
\newpage

For the second part of the experiment the best results came from using around \SI{50}{\gram} of room temperature distilled water that was poured into the previously used calorimeter. The weight of the water was calculated the same as before. Then above \SI{5}{\gram} of CaCl$_2$ that was properly weighed would be added to the water and after fully dissolving into the solution, the temperature was measured and used to calculate the $\Delta H_{rxn}$.


%----------------------------------------------------------------------------------------
%	SECTION 3
%----------------------------------------------------------------------------------------
\section{Data and Results}
Below are the measurements for two tests on the first part of the experiment using a milligram scale and a decadegrees Celsius thermometer.  
\\

\begin{table}[H]
	
	
	\begin{tabular}{l|r|r|}
		&{\bf Test 1}&{\bf Test 2}\\ \hline 
		Mass of styrofoam cup &  \SI{4.152}{\gram} & \SI{4.152}{\gram}\\ \hline
		Mass of cup with $water_{cold}$ & \SI{85.536}{\gram} & \SI{83.539}{\gram}\\ \hline
		Mass of $water_{cold}$ & \SI{83.536}{\gram} - \SI{4.152}{\gram}
		& \SI{83.539}{\gram} - \SI{4.152}{\gram}\\
		&= \SI{81.384}{\gram} 
		&= \SI{79.387}{\gram}\\ \hline
		Mass of cup with $water_{cold}$ & \SI{145.018}{\gram} & \SI{120.476}{\gram}
		\\and $water_{hot}$& & \\ \hline
		Mass of $water_{hot}$ & \SI{145.018}{\gram} - \SI{85.536} & \SI{120.427}{\gram} - \SI{83.539}{\gram}\\
		& = \SI{59.482}{\gram} & = \SI{36.937}{\gram}\\ \hline 
		Temperature of $water_{cold}$ & \SI{21.5}{\celsius} & \SI{21.5}{\celsius}\\ \hline
		Temperature of $water_{hot}$ & \SI{68.0}{\celsius} & \SI{67.9}{\celsius}\\ \hline
		Temperature of combined & \SI{40.1}{\celsius} & \SI{35.1}{\celsius}\\ \hline
	\end{tabular}
\end{table}

These values can the be placed into the equation of $0 = q_{rxn}+q_{solution}+q_{calorimeter}$. \\

{\bf Test 1}
\begin{equation}
		\begin{split}
			0 & = (\frac{mass_{rxn}}{g/mol_{rxn}} \times C_{p,rxn} \times \Delta T_{rxn}) + (\frac{mass_{solution}}{g/mol_{solution}} \times C_{p,solution} \times \Delta T_{solution} ) + (C_{cal} \times \Delta T_{cal})\\
			& = (\frac{\SI{81.384}{\gram}}{\SI{18.01}{\gram\per\mole}}\times 75.3_{\frac{J}{Kmol}}\times (\SI{40.1}{\celsius}-\SI{21.5}{\celsius})) + (\frac{\SI{59.482}{\gram}}{\SI{18.01}{\gram\per\mole}}\times 75.3_{\frac{J}{Kmol}}\times (\SI{40.1}{\celsius}-\SI{68.0}{\celsius}))\\ 
			& + (C_{cal}\times )(\SI{40.1}{\celsius}-\SI{21.5}{\celsius})\\
			& = 6328.972_J - 6938.586_J + 18.6^\circ \times C_{cal}\\
            609.614J & = 18.6^\circ \times C_{cal}\\
            32.775J/^\circ & = C_{cal}\\
						C_{cal} & = 32.8J/^\circ
	 \end{split}
\end{equation}

{\bf Test 2}
\begin{equation}
		\begin{split}
			0 & = (\frac{mass_{rxn}}{g/mol_{rxn}} \times C_{p,rxn} \times \Delta T_{rxn}) + (\frac{mass_{solution}}{g/mol_{solution}} \times C_{p,solution} \times \Delta T_{solution} ) + (C_{cal} \times \Delta T_{cal})\\
			& = (\frac{\SI{79.387}{\gram}}{\SI{18.01}{\gram\per\mole}}\times 75.3_{\frac{J}{Kmol}}\times (\SI{35.1}{\celsius}-\SI{21.5}{\celsius})) + (\frac{\SI{36.937}{\gram}}{\SI{18.01}{\gram\per\mole}}\times 75.3_{\frac{J}{Kmol}}\times (\SI{35.1}{\celsius}-\SI{67.9}{\celsius}))\\ 
			& + (C_{cal}\times )(\SI{35.1}{\celsius}-\SI{21.5}{\celsius})\\
			& = 4514.083_J - 5065.435_J + 15.0^\circ \times C_{cal}\\
			551.342J & = 13.6^\circ \times C_{cal}\\
			40.539J/^\circ & = C_{cal}\\
			C_{cal} & = 40.1/^\circ
	 \end{split}
\end{equation}

Giving the resulting values of 32.8 and 40.1 for the Ccal with an average of 36.8.\\



Next are the results for the second part of the experiment using $CaCl_2$.
\begin{table}[H]
	
	
	\begin{tabular}{l|r|r|r|r|}
		&{\bf Test 1}&{\bf Test 2}&{\bf Test 3}&{\bf Test 4}\\ \hline
		Mass of cup &  \SI{4.151}{\gram} & \SI{4.151}{\gram} & \SI{4.151}{\gram} & \SI{4.151}{\gram}\\ \hline
		Mass of water & \SI{133.258}{\gram} & \SI{103.498}{\gram} & \SI{67.185}{\gram} & \SI{54.224}{\gram}\\ \hline
		Mass of $CaCl_2$ & \SI{1.012}{\gram} & \SI{6.366}{\gram} & \SI{6.871}{\gram} & \SI{5.881}{\gram}\\ \hline
		Temperature of water & \SI{21.0}{\celsius} & \SI{21.0}{\celsius} & \SI{21.0}{\celsius} & \SI{21.0}{\celsius}\\ \hline
		Temperature of water$_rxn$  & \SI{22.1}{\celsius} & \SI{27.9}{\celsius} & \SI{34.5}{\celsius} & \SI{35.2}{\celsius}\\ \hline
	\end{tabular}
\end{table}


{\bf Theoretical $\Delta H_{rxn}$}

\begin{equation}
	\begin{split}
		\Delta H_{rxn} & = [(1\times-542.96) + (2\times-167.2)]-[(1\times-795.8]
		& = -81.56
	\end{split}
\end{equation}

{\bf Test 1}

\begin{equation}
		\begin{split}
			0 & = (\frac{mass_{rxn}}{g/mol_{rxn}} \times \Delta H_{rxn}) + (\frac{mass_{solution}}{g/mol_{solution}} \times \Delta H_{solution}^\circ \times \Delta T_{solution} ) + (C_{cal} \times \Delta T_{cal})\\
			& = (\frac{\SI{1.012}{\gram}}{\SI{110.9834}{\gram\per\mole}}\times \Delta H_{rxn}) + (\SI{134.27}{\gram} \times 4.186_{\frac{J}{g}}\times (\SI{22.1}{\celsius}-\SI{21.0}{\celsius}))\\ 
			&\phantom{{}=1}+ (36.8\times (\SI{22.1}{\celsius}-\SI{21.0}{\celsius}))\notag\\
			& = 0.009118_{mol} \times \Delta H_{rxn}+ 618.26_J + 40.59_J\\
			-658.95	& = 0.009118_{mol} \times \Delta H_{rxn}\\
			-72254.322_{\frac{J}{mol}} & = \Delta H_{rxn}\\
			\Delta H_{rxn}& = -72254.322{\frac{J}{mol}}\\
			\Delta H_{rxn}& = -72{\frac{kJ}{mol}}
	 \end{split}
 \end{equation}


{\bf Test 2}

\begin{equation}
		\begin{split}
			0 & = (\frac{mass_{rxn}}{g/mol_{rxn}} \times \Delta H_{rxn}) + (\frac{mass_{solution}}{g/mol_{solution}} \times \Delta H_{solution}^\circ \times \Delta T_{solution} ) + (C_{cal} \times \Delta T_{cal})\\
			& = (\frac{\SI{6.366}{\gram}}{\SI{110.9834}{\gram\per\mole}}\times \Delta H_{rxn}) + (\SI{109.864}{\gram} \times 4.186_{\frac{J}{g}}\times (\SI{27.9}{\celsius}-\SI{21.0}{\celsius}))\\ 
			&\phantom{{}=1}+ (36.8\times (\SI{27.9}{\celsius}-\SI{21.0}{\celsius}))\notag\\
			& = 0.057360_{mol} \times \Delta H_{rxn}+ 3173.25_J + 254.61_J\\
			-3427.86	& = 0.057360_{mol} \times \Delta H_{rxn}\\
			-59760.461_{\frac{J}{mol}} & = \Delta H_{rxn}\\
			\Delta H_{rxn}& = -59760.461{\frac{J}{mol}}\\
			\Delta H_{rxn}& = -60{\frac{kJ}{mol}}
	 \end{split}
 \end{equation}
 \newpage
 {\bf Test 3}

\begin{equation}
		\begin{split}
			0 & = (\frac{mass_{rxn}}{g/mol_{rxn}} \times \Delta H_{rxn}) + (\frac{mass_{solution}}{g/mol_{solution}} \times \Delta H_{solution}^\circ \times \Delta T_{solution} ) + (C_{cal} \times \Delta T_{cal})\\
			& = (\frac{\SI{6.871}{\gram}}{\SI{110.9834}{\gram\per\mole}}\times \Delta H_{rxn}) + (\SI{74.056}{\gram} \times 4.186_{\frac{J}{g}}\times (\SI{34.5}{\celsius}-\SI{21.0}{\celsius}))\\ 
			&\phantom{{}=1}+ (36.8\times (\SI{34.5}{\celsius}-\SI{21.0}{\celsius}))\notag\\
			& = 0.061910_{mol} \times \Delta H_{rxn}^+ 4184.98_J + 498.15_J\\
			-4683.1286	& = 0.061910_{mol} \times \Delta H_{rxn}\\
			-75643.94_{\frac{J}{mol}} & = \Delta H_{rxn}\\
			\Delta H_{rxn}& = -75643.94{\frac{J}{mol}}\\
			\Delta H_{rxn}& = -75.6{\frac{kJ}{mol}}
	 \end{split}
 \end{equation}


{\bf Test 4}

\begin{equation}
		\begin{split}
			0 & = (\frac{mass_{rxn}}{g/mol_{rxn}} \times \Delta H_{rxn}) + (\frac{mass_{solution}}{g/mol_{solution}} \times \Delta H_{solution}^\circ \times \Delta T_{solution} ) + (C_{cal} \times \Delta T_{cal})\\
			& = (\frac{\SI{5.881}{\gram}}{\SI{110.9834}{\gram\per\mole}}\times \Delta H_{rxn}) + (\SI{60.105}{\gram} \times 4.186_{\frac{J}{g}}\times (\SI{35.2}{\celsius}-\SI{21.0}{\celsius}))\\ 
			&\phantom{{}=1}+ (36.8\times (\SI{35.2}{\celsius}-\SI{21.0}{\celsius}))\notag\\
			& = 0.052990_{mol} \times \Delta H_{rxn}+ 3572.71_J + 523.98_J\\
			-4096.69	& = 0.052990_{mol} \times \Delta H_{rxn}\\
			-77310.82_{\frac{J}{mol}} & = \Delta H_{rxn}\\
			\Delta H_{rxn}& = -77310.82{\frac{J}{mol}}\\
			\Delta H_{rxn}& = -77.3{\frac{kJ}{mol}}
	 \end{split}
 \end{equation}


%----------------------------------------------------------------------------------------
%	SECTION 4
%----------------------------------------------------------------------------------------

\section{Discussion}

Ignoring Tests 1 and 2 due to significant figure errors, the average $\Delta H_{rxn}$ comes to -76.5. Yielding a percent error from our theoretical $\Delta H_{rxn}$ of -6.2\%. The results came in much closer the the theoretical value when using higher concentrations of NaCl$_2$. This is presumably because a much more noticable change would occur and produce more significant and precise results. The results were too high due to factors such as heat escaping into the air, an unprecise thermometer, and a not incredibly accurate calorimeter constant.\\ 

The accuracy of the results were heavily dependent on the precision used in the tests. The first two tests for the second part of the experiment contained low values that produced imprecise measurements that were unusable for the final outcome. The precision was in the end decided by the thermometer as it was the least precise tool for measurement. Meaning that if given a more precise thermometer, more precise results could have been yielded.



%----------------------------------------------------------------------------------------
%	SECTION 5
%----------------------------------------------------------------------------------------

\section{References}

{\emph Chemistry 110H Laboratory Manual}, 2016 ed.; Union College, Schenectady, NY; Expt. 1 pp.1-2.

%----------------------------------------------------------------------------------------
%	SECTION 6
%----------------------------------------------------------------------------------------

\section{Answers to Additional Problems}

\begin{equation}
		\begin{split}
			0 & = (\frac{mass_{rxn}}{g/mol_{rxn}} \times \Delta H_{rxn}) + (\frac{mass_{solution}}{g/mol_{solution}} \times \Delta H_{solution}^\circ \times \Delta T_{solution} ) + (1.20\timesC_{cal} \times \Delta T_{cal})\\
			& = (\frac{\SI{5.881}{\gram}}{\SI{110.9834}{\gram\per\mole}}\times \Delta H_{rxn}) + (\SI{60.105}{\gram} \times 4.186_{\frac{J}{g}}\times (\SI{35.2}{\celsius}-\SI{21.0}{\celsius}))\\ 
			&\phantom{{}=1}+ (1.20 \times 36.8\times (\SI{35.2}{\celsius}-\SI{21.0}{\celsius}))\notag\\
			& = 0.052990_{mol} \times \Delta H_{rxn} + 3572.71_J + 628.776_J\\
			-4201.4893	& = 0.052990_{mol} \times \Delta H_{rxn}\\
			-79288.483_{\frac{J}{mol}} & = \Delta H_{rxn}\\
			\Delta H_{rxn} & = -793277.483{\frac{J}{mol}}\\
			\Delta H_{rxn} & = -79.3{\frac{kJ}{mol}}
	 \end{split}
 \end{equation}
 \begin{enumerate}
	 \item{What percent comes from the calorimeter?}\\
		 $\frac{0.0368 \times 14.2}{-76.5} = .006831$
		 Around 0.683\% of $q_{rxn}$ comes from the calorimeter.

	 \item{What is the new q$_{rxn}$?\\
			 The new q$_{rxn}$ is -79.3${\frac{kJ}{mol}}$ from a 20\% change in the calorimeter (Calculation above, note that $\Delta H_{rxn}$ is the same as heat given that the pressure is constant).

		 \item{By what perecent has q$_{rxn}$ changed?}\\
			 q$_{rxn}$ has changed by $\frac{-79.3+76.5}{-76.5}$, or 3.66\%.

		 \item{How does our $C_{cal}$ calibration effect our experiment?}\\
			 Given that a 20\% error causes a 3.66\% change, errors would have very little effect.
 \end{enumerate}

%----------------------------------------------------------------------------------------
%	BIBLIOGRAPHY
%----------------------------------------------------------------------------------------


%----------------------------------------------------------------------------------------


\end{document}

