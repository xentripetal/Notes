#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Compiles latex from notes src into release."""

import os
import glob

__appname__ = "Tex Compile"
__author__ = "Corbin Martin (Xentripetal)"
__version__ = "0.1"
__license__ = "GNU GPL 3.0"

src_dir = "/home/xen/Documents/Notes"



def iterate_folder(path, ext):
    render_list = []
    for item in glob.iglob(path + "/**/*" + ext, recursive=True):
        render_list.append(item)
    return render_list


def render_all():
    render_list = iterate_folder(src_dir, ".tex")

    for render in render_list:
        render_file(render)


def render_new():
    render_list = iterate_folder(src_dir, ".tex")
    unparsed_compare_list = iterate_folder(src_dir, ".pdf")
    parsed_compare_list = []
    for item in unparsed_compare_list:
        parsed_compare_list.append(item.replace(".pdf", ".tex"))

    for render in render_list:
        if render not in parsed_compare_list:
            render_file(render)
    exit()


def render_single(args):
    render_list = []
    for arg in args:
        if arg[-4:] != ".tex":
            arg += ".tex"
        if os.path.isfile(src_dir + "/" + arg):
            render_list.append(src_dir + "/" + arg)
        else:
            print(src_dir + "/" + arg + " is not a file.")
    for render in render_list:
        render_file(render)


def render_file(path):
    filename = path.split('/')[-1].replace(".tex", ".pdf")
    newPath = path.replace(filename + ".tex", "pdf/" + filename + ".pdf")
    os.system("latexmk -pdf " + path)
    os.rename(filename,  newPath)
    print(newPath)
    for ext in [".log", ".aux", ".fls", ".thm", ".out", ".fdb_latexmk"]:
        os.remove(filename.replace('.pdf', ext))


if __name__ == '__main__':
    from optparse import OptionParser
    parser = OptionParser(version="%%prog v%s" % __version__,
                          usage="%prog [options] <file1> <file2> <file...>",
                          description=__doc__.replace('\r\n', '\n').split('\n--snip--\n')[0])
    parser.add_option('-a', '--all',
                      action="store_true",
                      dest="render_all",
                      default=False,
                      help="Re-render all files.")

    parser.add_option('-n', '--new',
                      action="store_true",
                      dest="render_new",
                      default=False,
                      help="Render all files that havent been rendered before")

    # Allow pre-formatted descriptions
    parser.formatter.format_description = lambda description: description
    (opts, args) = parser.parse_args()
    src_dir = os.path.abspath(os.path.join(os.path.dirname(__file__), '..'))

    if opts.render_all and not opts.render_new:
        if len(args) == 0:
            render_all()
        else:
            print("ERROR: Too many arguments. Render All takes no arguments.")
            parser.print_help()
    elif opts.render_new and not opts.render_all:
        if len(args) == 0:
            render_new()
        else:
            print("ERROR: Too many arguments. Render New takes no arguments.")
            parser.print_help()
    elif opts.render_new and opts.render_all:
        print("ERROR: You cannot render all and new. Please select one.")
    else:
        if len(args) != 0:
            render_single(args)
        else:
            print("ERROR: Not enough arguments. Atleast 1 file to render.")
            parser.print_help()

    print("Exiting...")

